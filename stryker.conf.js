module.exports = (config) => {
  config.set({
    mutator: {
      name: 'javascript',
      excludedMutations: [
        'StringLiteral',
        'ObjectLiteral'
      ]
    },
    packageManager: 'npm',
    reporters: ['clear-text', 'progress'],
    testRunner: 'command',
    commandRunner: { command: 'npm run unit-test' },
    transpilers: [],
    thresholds: { high: 60, low: 50, break: 60 },
    mutate: ['app/**/*.js', '!app/config/**', '!**/*spec.js'],
    files: ['./**/*', '!node_modules/**/*']
  });
};

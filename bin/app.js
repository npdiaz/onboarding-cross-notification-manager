if (process.env.NODE_ENV === 'production') {
  // eslint-disable-next-line
  require('@instana/collector')();
}

const express = require('express');
const bodyParser = require('body-parser');
const { errorController, notFoundController } = require('onboarding-linkage-common');
const route = require('../app/routes/route');

const app = express();
app.use(bodyParser.json({}));
app.set('trust proxy', true);
// se agrega limit porque se cae por store payload to large
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb', parameterLimit: 50000 }));

app.use('/onboarding/cross/notification', route());

// handle  not found  uris
app.use(notFoundController);
// handle  any extra error
app.use(errorController);

module.exports = app;

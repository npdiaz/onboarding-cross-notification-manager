pipeline {
    agent {
        node {
            label 'azure-slave'
        }
    }
    environment {
        AGENT_IMAGE = "hub.fif.tech/base/fif-common-pm2:12.10.0-3.5.0-2"
        SERVICE_NAME = "linkage-client-manager"
        MAIN_BRANCH = 'master'
        VAULT_ADDR = 'https://vault.fif.tech:8200'
        DOCKER_TERRAFORM_IMAGE ='hub.fif.tech/base/fif-terraform:3.0.0'
    }
    stages {
        stage('Download production dependencies') {
            agent {
                docker {
                    image '$AGENT_IMAGE'
                    args '-u root'
                    reuseNode true
                }
            }
            environment{
                NPM_PASSWORD = credentials('jenkins-npm')
            }
            steps{
              sh '''
                echo 'registry=https://npm-registry.fif.tech/' > .cnpmrc
                echo '//npm-registry.fif.tech/:_authToken="'$NPM_PASSWORD'"' >> .cnpmrc
                echo '@types:registry=https://registry.npmjs.org' >> .cnpmrc
                npm install --production --userconfig=.cnpmrc --no-optional
                rm .cnpmrc
              '''
          }
        }
        // stage('Check vulnerabilities') {
        //     steps {
        //     script {
        //         dependencyCheckAnalyzer hintsFile: '', includeCsvReports: false, includeHtmlReports: true, includeJsonReports: false, isAutoupdateDisabled: true, outdir: '', scanpath: 'package.json', skipOnScmChange: false, skipOnUpstreamChange: false, suppressionFile: '', zipExtensions: ''
        //         dependencyCheckPublisher canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '', thresholdLimit: 'high', unHealthy: '', unstableTotalAll: '10', unstableTotalHigh: '1', unstableTotalNormal: '4'
        //     }
        //     }
        // }
        // stage('Download dependencies') {
        //     agent {
        //         docker {
        //             image '$AGENT_IMAGE'
        //             args '-u root'
        //             reuseNode true
        //         }
        //     }
        //     environment{
        //         NPM_PASSWORD = credentials('jenkins-npm')
        //     }
        //     steps{
        //       sh '''
        //         echo 'registry=https://npm-registry.fif.tech/' > .cnpmrc
        //         echo '//npm-registry.fif.tech/:_authToken="'$NPM_PASSWORD'"' >> .cnpmrc
        //         echo '@types:registry=https://registry.npmjs.org' >> .cnpmrc
        //         npm install --userconfig=.cnpmrc --no-optional
        //         rm .cnpmrc
        //       '''
        //   }
        // }
        // stage('eslint') {
        //     agent {
        //         docker {
        //             image '$AGENT_IMAGE'
        //             args '-u root'
        //             reuseNode true
        //         }
        //     }
        //     steps{
        //         sh '''
        //             npm run eslint
        //         '''
        //     }
        // }
        // stage('Unit Test') {
        //     agent {
        //         docker {
        //             image '$AGENT_IMAGE'
        //             args '-u root'
        //             reuseNode true
        //         }
        //     }
        //     steps{
        //         sh '''
        //             npm run unit-test
        //         '''
        //     }
        // }
        // stage('Coverage') {
        //     agent {
        //         docker {
        //             image '$AGENT_IMAGE'
        //             args '-u root'
        //             reuseNode true
        //         }
        //     }
        //     steps{
        //         sh '''
        //             npm run coverage
        //         '''
        //     }
        // }
        // stage('Mutation Test') {
        //     agent {
        //         docker {
        //             image '$AGENT_IMAGE'
        //             args '-u root'
        //             reuseNode true
        //         }
        //     }
        //     steps{
        //         sh '''
        //             npm run mutation-test
        //         '''
        //     }
        // }
        // stage('Functional Test') {
        //     agent {
        //         docker {
        //             image '$AGENT_IMAGE'
        //             args '-u root'
        //             reuseNode true
        //         }
        //     }
        //     steps{
        //         sh '''
        //             npm run functional-test
        //         '''
        //     }
        // }
        // stage('SonarQube analysis') {
        //     steps{
        //         withSonarQubeEnv('sonar') {
        //             sh '/opt/sonar/bin/sonar-runner'
        //         }
        //     }
        // }
        stage('Prepare for docker') {
            when {
                branch env.MAIN_BRANCH
            }
            agent {
                docker {
                    image '$AGENT_IMAGE'
                    args '-u root'
                    reuseNode true
                }
            }
            steps{
                sh '''
                    npm prune --production
                '''
            }
        }
        stage('Calculate Tag') {
            when {
                branch env.MAIN_BRANCH
            }
            steps {
                script {
                    env.GIT_TAG = sh(returnStdout: true, script: 'echo $(date +%Y%m%d)-$(date +%H%M%S)-BUILD').trim()
                    env.IMAGE_TAG = "$SERVICE_NAME:$GIT_TAG"
                    env.REGISTRY_TAG = "hub.fif.tech/onboarding/$IMAGE_TAG"
                }
            }
        }
        stage('Vault get Credentials') {
            when {
            branch env.MAIN_BRANCH
            }
            agent {
            docker {
                image '$DOCKER_TERRAFORM_IMAGE'
                args '-u root --entrypoint="" -e VAULT_ADDR=$VAULT_ADDR'
                reuseNode true
            }
            }
            steps {
            withCredentials([usernamePassword(credentialsId: 'banco-cl-onboarding-test-pipeline', passwordVariable: 'SECRET_ID', usernameVariable: 'ROLE_ID')]) {
                script {
                env.VAULT_TOKEN = sh(returnStdout: true, script: 'vault write -field=token auth/approle/login role_id=${ROLE_ID} secret_id=${SECRET_ID}').trim()
                sh '''
                    vault read -field=auth_config kv/clusters/banco-cl-onboarding-test-cluster/shared/automated/harbor-onboarding > config.json
                '''
                }
            }
            }
        }
        stage('Building Docker Image') {
            when {
                branch env.MAIN_BRANCH
            }
            steps{
                sh '''
                    docker build --rm=true -t "$IMAGE_TAG" -f ${WORKSPACE}/dockerfiles/Dockerfile . --build-arg BUILDNUMBER=$GIT_TAG --label version=$GIT_TAG
                '''
            }
        }
        // stage('Anchore Analysis') {
        //     when {
        //         branch env.MAIN_BRANCH
        //     }
        //     agent {
        //         docker {
        //             image 'anchore/jenkins'
        //             reuseNode true
        //         }
        //     }
        //     steps{
        //         sh '''
        //             echo "$IMAGE_TAG" > image.txt
        //         '''
        //         anchore bailOnWarn: false, inputQueries: [
        //             [query: 'cve-scan all'], [query: 'list-packages all'],
        //             [query: 'list-files all'], [query: 'show-pkg-diffs base']
        //         ], name: 'image.txt'
        //     }
        // }
        stage('Publish Docker Image') {
            when {
                branch env.MAIN_BRANCH
            }
            steps{
                sh '''
                    export DOCKER_CONFIG=$WORKSPACE
                    docker tag $IMAGE_TAG $REGISTRY_TAG
                    docker push $REGISTRY_TAG
                    docker rmi $IMAGE_TAG
                    docker rmi $REGISTRY_TAG
                '''
            }
        }
        stage ('Git TAG') {
            when {
                branch env.MAIN_BRANCH
            }
            steps {
                sh("git tag -f -a $GIT_TAG -m 'version $GIT_TAG'")
                sshagent(['11f132ab-f4f6-4c0e-9c87-8723dc0e47e7']) {
                    sh("git push -f --tags ")
                }
            }
        }
    }
    post {
        cleanup {
            echo 'Clean up workspace'
            cleanWs()
            deleteDir()
            script {
                docker.image(env.AGENT_IMAGE).inside('-u root') {
                    sh('find ${WORKSPACE} -mindepth 1 -delete')
                }
            }
        }
        success {
            echo 'Bitbucket notify SUCCESSFUL'
            bitbucketStatusNotify ( buildState: 'SUCCESSFUL' )
        }
        failure {
            echo 'Bitbucket notify FAILED'
            bitbucketStatusNotify ( buildState: 'FAILED' )
        }
        aborted {
            echo 'Bitbucket notify FAILED'
            bitbucketStatusNotify ( buildState: 'FAILED' )
        }
    }
}
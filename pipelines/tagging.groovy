#!groovy

try {
  node('azure-slave') {
    env.REGISTRY_IMAGE = 'hub.fif.tech/onboarding/linkage-client-manager'
    env.DOCKER_TERRAFORM_IMAGE ='hub.fif.tech/base/fif-terraform:3.0.0'
    env.VAULT_ADDR = 'https://vault.fif.tech:8200'
    stage('Clean Workspace') {
      sh("find ${WORKSPACE} -mindepth 1 -delete")
      docker.image('$DOCKER_TERRAFORM_IMAGE').inside('-u root --entrypoint=""'){
        sh('find ${WORKSPACE} -mindepth 1 -delete')
      }
    }
    stage('Vault get Credentials') {
      docker.image('$DOCKER_TERRAFORM_IMAGE').inside('-u root --entrypoint="" -e VAULT_ADDR=$VAULT_ADDR') {
        withCredentials([usernamePassword(credentialsId: "${CREDENTIALS_ID}", passwordVariable: 'SECRET_ID', usernameVariable: 'ROLE_ID')]) {
          script {
            env.VAULT_TOKEN = sh(returnStdout: true, script: 'vault write -field=token auth/approle/login role_id=${ROLE_ID} secret_id=${SECRET_ID}').trim()
            sh '''
              vault read -field=auth_config kv/clusters/${CLUSTER_NAME}/shared/automated/harbor-onboarding > config.json
            '''
          }
        }
      }
    }
    stage('Retag image') {
      sh '''
        export DOCKER_CONFIG=$WORKSPACE
        docker pull ${REGISTRY_IMAGE}:${REGISTRY_TAG}
        docker tag ${REGISTRY_IMAGE}:${REGISTRY_TAG} ${REGISTRY_IMAGE}:${TARGET_TAG}
        docker push ${REGISTRY_IMAGE}:${TARGET_TAG}
        docker rmi ${REGISTRY_IMAGE}:${REGISTRY_TAG}
        docker rmi ${REGISTRY_IMAGE}:${TARGET_TAG}
      '''
    }
    stage ('Deploying job') {
      try {
        DEPLOY = "${ENABLE_DEPLOY}"
      } catch (error) {
        DEPLOY = 'YES'
      }
      echo ("ENABLE_DEPLOY: ${DEPLOY}")
      if ("NO" != "${DEPLOY}") {
        def jobName = "${TARGET_TAG}".replaceAll('-', '/')
        jobName = "${jobName}".replaceAll('dev', 'stack-dev')
        jobName = "${jobName}".replaceAll('qa', 'stack-qa')
        jobName = "${jobName}".replaceAll('prod', 'stack-prod')
        build job: "${jobName}"
      }
    }
  }
} catch(e) {
  currentBuild.result = 'FAILURE'  
  throw e
} finally {
  node('azure-slave') {
    stage('Clean Workspace') {
      sh("find ${WORKSPACE} -mindepth 1 -delete")
      docker.image('$DOCKER_TERRAFORM_IMAGE').inside('-u root --entrypoint=""'){
        sh('find ${WORKSPACE} -mindepth 1 -delete')
      }
    }
    stage('Summary') {  
      echo("Build number: ${REGISTRY_TAG}, ${REGISTRY_IMAGE}:${TARGET_TAG}")
    }
  }
}

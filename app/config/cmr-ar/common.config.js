module.exports = {
  serviceName: 'onboarding-cross-notification-manager',
  loadBalancerAddress: 'http://172.17.0.1',
  liaNotificationEndpoint: 'https://lia-qa-priv.fif.tech/notifications/api/send/async/',
  storeSaveEndpoint: ':5301/onboarding/linkage/store/v1/save-data',
  applicationManagerEndpoint: ':7000/onboarding/application/cliente-solicitud-admision-actualizar-ar',
};

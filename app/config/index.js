const env = process.env.NODE_ENV || 'local';
const envApp = process.env.NODE_ENV_APP;
const config = require(`./${envApp}/${env}.config`); // eslint-disable-line import/no-dynamic-require
const commonConfig = require(`./${envApp}/common.config`); // eslint-disable-line import/no-dynamic-require

const salida = Object.assign(commonConfig, config);
module.exports = Object.freeze(salida);

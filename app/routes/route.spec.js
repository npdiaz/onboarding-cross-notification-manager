/* eslint-disable global-require */
const mockery = require('mockery');
const { expect } = require('chai');


describe('route', () => {
  const cors = ({ methods }) => {
    expect(methods).to.be.eql(['GET', 'POST', 'HEAD', 'OPTIONS']);
  };
  let triggeredPost = false;
  const express = {
    Router() {
      const post = (...args) => {
        triggeredPost = true;
        const generalMiddlewares = args[1];
        const finalMiddlewares = args[args.length - 1];
        const liaNotification = args[args.length - 2];
        expect(generalMiddlewares.length).not.to.be.equal(0);
        expect(liaNotification.length).not.to.be.equal(0);
        expect(finalMiddlewares.length).not.to.be.equal(0);
      };
      return { use() {}, get() {}, post };
    }
  };
  beforeEach(() => {
    mockery.enable({ warnOnReplace: false, warnOnUnregistered: false, useCleanCache: true });
    mockery.registerMock('express', express);
    mockery.registerMock('cors', cors);
  });
  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
    delete process.env.NODE_ENV_APP;
  });
  
  it('should load router', () => {
    process.env.NODE_ENV_APP = 'cmr-ar';
    const route = require('./route');
    const router = route();
    expect(router).not.to.be.equal(undefined);
    expect(triggeredPost).to.be.equal(true);
  });
});

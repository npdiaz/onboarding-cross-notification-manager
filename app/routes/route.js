/* eslint-disable global-require */
const express = require('express');
const allowCrossDomain = require('cors');
const { healthControllerCreator, loggerMiddlewareCreator,
  storeGetMiddleware, storeSaveInputMiddleware, storeUpdateMiddleware, responseController, enqueueMiddleware, storeSaveOutputMiddleware } = require('onboarding-linkage-common');
const { serviceName } = require('../config');

const loggerMiddleware = loggerMiddlewareCreator(serviceName);
const healthController = healthControllerCreator(serviceName);
const commerceCountry = process.env.NODE_ENV_APP;
const generalMiddlewares = [loggerMiddleware, storeGetMiddleware, storeSaveInputMiddleware];
const finalMiddlewares = [storeSaveOutputMiddleware, storeUpdateMiddleware, enqueueMiddleware, responseController];

const exposeEndpoints = {
  'cmr-ar': (router) => {
    const liaNotificationMiddleware = require('../middleware/cmr-ar/lia-notification-middleware');
    router.post('/lia-notification', generalMiddlewares, liaNotificationMiddleware, finalMiddlewares);
  }
};

module.exports = () => {
  const router = express.Router();
  router.use(allowCrossDomain({
    methods: ['GET', 'POST', 'HEAD', 'OPTIONS']
  }));
  router.get('/health', healthController);
  exposeEndpoints[commerceCountry](router);
  return router;
};

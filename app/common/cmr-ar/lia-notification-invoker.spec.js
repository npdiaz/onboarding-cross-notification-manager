/* eslint-disable global-require */
const mockery = require('mockery');
const { expect } = require('chai');
const { get } = require('onboarding-linkage-common');

describe('notification-invoker', () => {
  const getReq = () => {
    const request = {
      liaNotificationCheckbox: 'true'
    };
    const store = {
      locals: {
        store: {
          assessment: {
            cross: {
              application: {
                create: {
                  request: {
                    cliente: {
                      nombres: 'Nicole Patricia',
                      informacionContacto: {
                        telefono: {
                          numeroTelefono: '974996457'
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    };
          
    return { locals: { store }, headers: {} };
  };
  const getCommonMock = () => {
    const restInvoker = ({ body }) => {
      return Promise.resolve();
    };
    return { restInvoker, get };
  };
  beforeEach(() => {
    mockery.enable({ warnOnReplace: false, warnOnUnregistered: false, useCleanCache: true });
  });
  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });
  it('should trigger restInvoker', (done) => {
    const commonMock = getCommonMock();
    mockery.registerMock('onboarding-linkage-common', commonMock);
    const req = getReq();
    const notificationInvoker = require('./lia-notification-invoker');
    notificationInvoker(req).then(() => {
      expect(req.locals.store.linkage.cross.notification).to.be.equal('hola');
      done();
    });
  });
 
});

const { restInvoker, get } = require('onboarding-linkage-common');
// const request = require('request');
const { loadBalancerAddress, liaNotificationEndpoint } = require('../../config');

module.exports = (req) => { 
  const url = liaNotificationEndpoint;
  const template = 'template-cmr-ar';
  const nombre = get('req.locals', 'store.assessment.cross.application.create.request.cliente.nombres');
  const name = nombre.substr(0, nombre.indexOf(' ')) ? nombre.substr(0, nombre.indexOf(' ')) : nombre;
  const phone = get('req.locals', 'store.assessment.cross.application.create.request.cliente.informacionContacto.telefono.numeroTelefono');
  const telefono = `54${phone}`;
  const perfil = { id: telefono, id_type: 'WS' };
  const body = {
    template_slug: template,
    profile: perfil,
    value: { params: [name] } //agregar resto de params
  };

  const headers = {
    Authorization: 'Token {jwt_token}',//generar token 
    'Content-Type': 'application/json'
  };
  if (!req.locals.store.linkage.notification) {
    req.locals.store.linkage.cross.notification = {};
  }
  req.locals.store.linkage.cross.notification.liaNotification = [{ request: body }];
  req.headers = headers;
  return restInvoker({ req, url, body });
      
  //   const settings = { //PARA LLAMADO CON METODO REQUEST
  //     headers: req.headers,
  //     timeout: 20000,
  //     json: req.body
  //   };
  //   request.post(liaNotificationEndpoint, settings, (error, response) => {
  //     if (error) {
  //       return reject(error);
  //     }
  //     return fulfill(response);
  //   });
  // return fulfill(undefined);
};



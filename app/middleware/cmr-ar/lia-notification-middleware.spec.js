/* eslint-disable global-require */
const mockery = require('mockery');
const { expect } = require('chai');

describe('lia-notification-middleware', () => {
  const getNotificationInvokerMock = (statusCode, body, error) => {
    const notificationInvoker = (req) => {
      req.locals.store.linkage.notification = {};
      req.locals.store.linkage.notification.liaNotification = [{ request: {} }];
      return error ? Promise.reject(error) : Promise.resolve({ statusCode, body });
    };
    return notificationInvoker;
  };
  beforeEach(() => {
    mockery.enable({ warnOnReplace: false, warnOnUnregistered: false, useCleanCache: true });
  });
  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });
  it('should skip if phone is not in whitelist', (done) => {
    const liaNotificationMiddleware = require('./lia-notification-middleware');
    const req = {
      locals: {
        store: {
          assessment: {
            cross: {
              application: {
                create: {
                  request: {
                    cliente: {
                      informacionContacto: {
                       telefono: {
                         numeroTelefono: '974996457'
                       }
                      }
                    }
                  }
                }
              }
            }
          },
          linkage: {}
        }
      }
    };
    liaNotificationMiddleware(req, undefined, () => {
      expect(req.locals.liaNotificationData).to.be.equal(undefined);
      done();
    });
  });
 
  it('should skip if phone in whitelist', (done) => {
    mockery.registerMock('../../common/cmr-ar/notification-invoker', getNotificationInvokerMock({ "event_id": '200' }, { estadoOperacion: { codigoOperacion: '00' } }));
    const liaNotificationMiddleware = require('./lia-notification-middleware');
    const req = {
      locals: {
        store: {
          assessment: {
            cross: {
              application: {
                create: {
                  request: {
                    cliente: {
                      nombres: 'Nicole Patricia',
                      informacionContacto: {
                       telefono: {
                         numeroTelefono: '975550538'
                       }
                      }
                    }
                  }
                }
              }
            }
          },
          linkage: {}
        }
      }
    };
    liaNotificationMiddleware(req, undefined, () => {
      expect(req.locals.store.linkage.notification.liaNotification.response).to.be.deep.equal('hola');
      expect(req.locals.liaNotificationData).to.be.deep.equal('hola');
      done();
    });
  });

});

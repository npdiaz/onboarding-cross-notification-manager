const { Logger, get } = require('onboarding-linkage-common');
const notificationInvoker = require('../../common/cmr-ar/lia-notification-invoker');
const env = process.env.NODE_ENV || 'local';
const phoneWhiteList = [ '56975550538' ];

module.exports = (req, res, next) => { //Colocar async
  const logger = new Logger('NOTIFICATION-MIDDLEWARE', req);
  const phone = get('req.locals', 'store.assessment.cross.application.create.request.cliente.informacionContacto.telefono.numeroTelefono');
  const telefono = `56${phone}`; //cambiar a 54

  if (env !== 'production' && !(telefono in phoneWhiteList) ) {  
    logger.info('api lia not called');
    return next();
  }

  return notificationInvoker(req)  //COLOCAR await
    .then(({ body: response, statusCode }) => {
      const bodyAsString = JSON.stringify(response);
      logger.info('Response from api lia', { statusCode, bodyAsString });
      if (response !== { "event_id": '200' }) {
        req.locals.store.linkage.cross.notification.liaNotification.error = response;
      }
      else {
        req.locals.store.linkage.cross.notification.liaNotification.response = response;
        req.locals.liaNotificationData = response;
      }
      return next();
    })
    .catch((error) => {
      logger.error(error.stack);
      req.locals.store.linkage.cross.notification.liaNotification.error =  error.message; 
      return next();
    });
};
